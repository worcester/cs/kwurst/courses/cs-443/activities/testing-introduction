package edu.worcester.cs;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Example of method order based on annotations - has nothing to do with Rectangles")
public class AnnotationTest {
    @AfterAll
    static void method1() {
            System.out.println("AfterAll");
    }

    @AfterEach
    void method2() {
            System.out.println("AfterEach");
    }

    @BeforeAll
    static void method3() {
            System.out.println("BeforeAll");
    }

    @BeforeEach
    void method4() {
            System.out.println("BeforeEach");
    }

    @Test
    void method6() {
            System.out.println("method6 Test");
    }
    
    @Test
    void method5() {
            System.out.println("method5 Test");
    }

    @Test
    void method7() {
            System.out.println("method7 Test");
    }
}